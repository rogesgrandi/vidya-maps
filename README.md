# Vidya Maps

**1. O que é o Vidya Maps?**
O Vidya Maps é um editor de mapas conceituais livre e gratuito, distribuído sob [licença GPL](http://www.gnu.org/licenses/gpl-3.0.html). Pode ser baixado e utilizado em notebooks, chromebooks e computadores desktop nos seguintes navegadores Web: **Firefox, Chrome e Opera**. No navegador Safari, o mapa foi exibido com sucesso, mas com problemas de edição. No navegador Microsoft Edge, o mapa não foi exibido com sucesso. É possível, também, visualizar os mapas conceituais em celulares e tablets, mas editá-los nesses dispositivos é inviável na versão atual.

**2. Como obter e usar?**

O Vidya Maps pode ser clonado ou baixado como um arquivo compactado. Se optar por baixar um arquivo compactado (**vidya-maps-master.zip**, por exemplo), descompacte-o. Seja clonando ou descompactando o arquivo, será gerado o diretório vidya-maps-master. Dentro desse diretório, tem o arquivo **index.html**. Abra esse arquivo em um dos navegadores Web recomendados e o Vidya Maps estará pronto para uso.

**3. Barra de Ferramentas do Vidya Maps**
 
A barra de ferramentas Maps tem 8 botões: “Abrir”, “Salvar”, “Fotografar”, “Imprimir”, Sumário e Métricas, “Ajuda”, “Aumentar zoom” e “Diminuir zoom”.

**3.1 Botão “Salvar”**
Uma vez que você tenha desenvolvido seu mapa conceitual, poderá salvá-lo localmente no computador em que estiver operando. A extensão de um arquivo **Vidya Maps** é **vm1**. Exemplo: mapa.vm1.

**3.2 Botão “Abrir”** 
Abre um mapa conceitual salvo no computador em que estiver operando.

**3.3 Botão “Fotografar”**
Gera uma imagem no formato PNG do mapa conceitual construído.

**3.4 Botão “Imprimir”** 
Gera um documento PDF do mapa conceitual construído com as propriedades informadas, seu sumário e métricas.

**3.5 Botão “Sumário e Métricas”** 
Exibe ou esconde a sessão de Sumário e Métricas do mapa conceitual.

**3.6 Botão “Ajuda”** 
Exibe em uma nova aba do navegador a página de ajuda do **Vidya Maps**.

**3.7 Botão “Aumentar Zoom”** 
Aumenta o tamanho das fontes até um zoom de 150%.

**3.8 Botão “Diminuir Zoom”** 
Diminui o tamanho das fontes até um zoom de 100%.
