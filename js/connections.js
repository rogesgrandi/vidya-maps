class Metrics {
    numberPropositions = 0;
    differentConcepts = 0;
    differentWords = 0;
    //conceptsNotWritten = 0;
    isolateConcepts = 0;
    rootConcepts = 0;
    undefinedTextConcepts = 0;
    undefinedTextPhrases = 0;
    connections = 0;
    //mainConceptTarget = 0;
    complexity = 0;
}

window.metrics = new Metrics();

class NodeMap {
    useId = "";
    rectId = "";
    textId = "";
    content = "";
    in = 0;
    out = 0;
}

class Concept extends NodeMap {
}

class Phrase extends NodeMap {
}

/**
 * source: Source node. May be a concept or a phrase.
 * target: Target node. May be a concept or a phrase.
 * lineId: Line that connects source and target nodes.
 */
class Connection {
    sourceId = "";
    targetId = "";
    lineId = "";
}

window.countConnections = function() {
    
    const json = getJsonConnections();
    
    window.metrics.connections = json.length;
}

window.countIsolateConcepts = function() {

    const jsonConcepts = getJsonConcepts();
    const jsonConnections = getJsonConnections();

    var amount = 0;

    // console.log("countIsolateConcepts: jsonConcepts.length", jsonConcepts.length);
    // console.log("countIsolateConcepts: jsonConnections.length", jsonConnections.length);

    for (let i = 0; i < jsonConcepts.length; i++) {

        var concept = jsonConcepts[i];
        //console.log("countIsolateConcepts: concept", concept);
        var useId = concept.useId;
        var found = false;
        //console.log("countIsolateConcepts: concept.useId", concept.useId);
        for (let j = 0; j < jsonConnections.length; j++) {

            var connection = jsonConnections[j];
            if ((connection.sourceId === useId) || (connection.targetId === useId)) {
                found = true;
                break;
            }
        }

        if (!found) {
            //console.log("countIsolateConcepts: not found useId", useId);
            amount++;
        }
    }

    //console.log("countIsolateConcepts: amount", amount);
    window.metrics.isolateConcepts = amount;
}

window.isTarget = function(nodeId) {

    //console.log("countRootConcepts: nodeId", nodeId);
    
    var json = getJsonConnections();
    for (let i = 0; i < json.length; i++) {
        let connection = json[i];
        if (connection.targetId === nodeId) {
            //console.log("countRootConcepts: found target", nodeId);
            return true;
        }
    }

    //console.log("countRootConcepts: is not target", nodeId);
    return false;
}

window.countRootConcepts = function() {

    var conceptPrincipal = document.getElementById("text_concept_1");
    
    if (isEmpty(conceptPrincipal)) {
        return 0;
    }

    var principalUpper = conceptPrincipal.textContent.toUpperCase(); 

    var amount = 0;
    const jsonConcepts = getJsonConcepts();
    const jsonConnections = getJsonConnections();
    //Iterate concepts
    for (let i = 0; i < jsonConcepts.length; i++) {

        var concept = jsonConcepts[i];
        var upper = concept.upper;
        
        //do not consider principal concept
        if (principalUpper === upper) {
            continue;
        }

        //iterate connections
        var useId = concept.useId;
        for (let j = 0; j < jsonConnections.length; j++) {

            var connection = jsonConnections[j];
            if (connection.sourceId === useId) {
                if (!isTarget(useId)) {
                    amount++
                    // console.log("countRootConcepts: is root: ", useId)
                    // console.log("countRootConcepts: amount: ", amount)
                    break; 
                }
            }
        }
    }

    //console.log("countRootConcepts: final amount: ", amount)
    window.metrics.rootConcepts = amount;
}

window.countUndefinedTextConcepts = function() {

    const jsonConcepts = getJsonConcepts();

    var amount = 0;

    for (let i = 0; i < jsonConcepts.length; i++) {

        var concept = jsonConcepts[i];

        if (concept.content === ".....") {
            amount++;
        }
    }

    window.metrics.undefinedTextConcepts = amount;
}

window.adjustLines = function(selectedElement) {
    var centerSelected = svgGetCenter(selectedElement);
    let lines = getSourceTargetLines(selectedElement);
    moveLines(lines, centerSelected.x, centerSelected.y);
}

window.countUndefinedTextPhrases = function() {

    const connections = getJsonConnections();

    var amount = 0;

    var text;
    for (let i = 0; i < connections.length; i++) {

        var connection = connections[i];
        if (connection.sourceId.includes("phrase")) {
            text = getTextFromUseId(connection.sourceId);
            if (text.textContent === ".....") {
                amount++;
            }
        } else if (connection.targetId.includes("phrase")) {
            text = getTextFromUseId(connection.targetId);
            if (text.textContent === ".....") {
                amount++;
            }
        }
    }


    window.metrics.undefinedTextPhrases = amount;
}

window.deleteMapNode = function(element) {

    var json = getJsonMapNodes();
    var array = new Array();
    
    for (let i = 0; i < json.length; i++) {        
        let item = json[i];
        if (element.id !== item) {
            array.push(item);
        }
    }

    var sNodes = JSON.stringify(array);
    document.getElementById("mapNodes").textContent = sNodes;
}

window.getArrayMapNodes = function() {
    
    var json = getJsonMapNodes();
    var array = new Array();
    
    for (let i = 0; i < json.length; i++) {        
        let item = json[i];
        array.push(item);
    }
    
    return array;
}

window.getJsonMapNodes = function() {
    const mapNodes = document.getElementById("mapNodes");
    return JSON.parse(mapNodes.textContent);
}

window.getArrayConcepts = function() {

    var json = getJsonConcepts();
    var array = new Array();
    for (let i = 0; i < json.length; i++) {        
        let item = json[i];
        array.push(item);
    }

    return array;
}

window.getJsonConcepts = function() {
    const connections = document.getElementById("concepts");
    return JSON.parse(connections.textContent);
}

window.getArrayConnections = function() {

    var json = getJsonConnections();
    var array = new Array();
    for (let i = 0; i < json.length; i++) {        
        let item = json[i];
        array.push(item);
    }

    return array;
}

window.getJsonConnections = function() {
    const connections = document.getElementById("connections");
    return JSON.parse(connections.textContent);
}

window.countDifferentConcepts = function() {

    const json = getJsonConcepts();

    var set = new Set();
    
    for (let i = 0; i < json.length; i++) {

        let concept = json[i];
        let content = concept.content.toUpperCase();
        if (!set.has(content)) {
            set.add(content);
        }
        //console.log("countDifferentConcepts: set", set);
    }

    window.metrics.differentConcepts = set.size;
}

window.updateTextWords = function(force) {

    let mapTextBack = document.getElementById("mapTextBack");
    let content = getMapTextContent();

    if ((force === undefined) || !force || (mapTextBack.innerHTML === content)) {
        return;
    } 
    mapTextBack.innerHTML = content;

    var wordsSet = new Set();
    content = extractMapTextWords(content);
    let text = "";
    //window.metrics.conceptsNotWritten = 0; 
    var upper;
    console.log("updateTextWords: content", content);
    if (!isEmpty(content)) {

        var words = content.split(" ");
        for (var i = 0; i < words.length; i += 1) {

            let word = words[i];
            if (word === "") {
                continue;
            }
            //console.log("updateTextWords: word >"+word+"<");
            upper = word.toUpperCase();
            if (!wordsSet.has(upper)) {
                wordsSet.add(upper);
                text += word + ", ";        
            }
        }

        // upper = content.toUpperCase();
        // console.log("updateTextWords: upper", upper);

        // var json = getJsonConcepts();
        // var set = new Set();
        // for (let i = 0; i < json.length; i++) {
            
        //     let concept = json[i];
        //     let content = concept.content.toUpperCase();
        //     if (!set.has(content)) {
        //         set.add(content);
        //         if (upper.indexOf(content) < 0) {
        //             window.metrics.conceptsNotWritten++; 
        //         }

        //     }
        // }
        //console.log("updateTextWords: conceptsNotWritten", window.metrics.conceptsNotWritten);
    }

    if (text.length > 1) {
        text = text.slice(0, -2);
    }

    //console.log("updateTextWords: wordsSet", wordsSet);
    window.metrics.differentWords = wordsSet.size;
    //adjustTextArea("textWords", text);
}

window.getConceptId = function(name) {

    const mapNodes = getArrayMapNodes();

    for (let i = 0; i < mapNodes.length; i++) {
    
        var mapNode = mapNodes[i];

        if (mapNode.startsWith("use_concept")) {

            const text = getTextFromUseId(mapNode);

            if (null !== text) {

                const content = text.textContent;
                if (content.toUpperCase() === name.toUpperCase()) {
                    return mapNode; 
                }
            }
        }
    }

    return;
}

window.getConceptPhraseConnection = function(conceptFrom, phrase) {

    const jsonConnections = getJsonConnections();

    for (let j = 0; j < jsonConnections.length; j++) {

        var connection = jsonConnections[j];
        var sourceId = connection.sourceId;
        var targetId = connection.targetId;

        if (sourceId.includes("use_concept") && 
            targetId.includes("use_phrase")) {
            
            var conceptText = getTextFromUseId(sourceId).textContent;
            var phraseText = getTextFromUseId(targetId).textContent;
            //console.log("getConceptPhraseConnection concept text", conceptText);
            //console.log("getConceptPhraseConnection phrase text", phraseText);

            if ((conceptText.toUpperCase() === conceptFrom.toUpperCase()) &&
                (phraseText.toUpperCase() == phrase.toUpperCase())) {
                console.log("getConceptPhraseConnection found connection", connection);
                return connection;
            }
        }
    }

    return;
}


window.getPhraseConceptConnection = function(phrase, conceptTo) {

    const jsonConnections = getJsonConnections();

    for (let j = 0; j < jsonConnections.length; j++) {

        var connection = jsonConnections[j];
        var sourceId = connection.sourceId;
        var targetId = connection.targetId;

        if (sourceId.includes("use_phrase") && 
            targetId.includes("use_concept")) {
            
            var phraseText = getTextFromUseId(sourceId).textContent;
            var conceptText = getTextFromUseId(targetId).textContent;
            //console.log("getPhraseConceptConnection phrase text", phraseText);
            //console.log("getPhraseConceptConnection concept text", conceptText);

            if ((phraseText.toUpperCase() == phrase.toUpperCase()) &&
                (conceptText.toUpperCase() === conceptTo.toUpperCase())) {      
                    console.log("getPhraseConceptConnection found connection", connection);
                    return connection;
            }
        }
    }

    return;
}

window.addProposition = function() {

    var sourceConceptAdd = document.getElementById("sourceConceptAdd");
    var phraseAdd        = document.getElementById("phraseAdd");
    var targetConceptAdd = document.getElementById("targetConceptAdd");

    const from   = sourceConceptAdd.value.trim();
    const phrase = phraseAdd.value.trim();
    const to     = targetConceptAdd.value.trim();

    sourceConceptAdd.value = "";
    phraseAdd.value = "";
    targetConceptAdd.value = "";    

    console.log("addProposition", from, phrase, to);

    /*
    * If from, phrase or to is empty do not add the proposition.
    */
    if (from === "") {
        console.log("addProposition empty sourceConceptAdd");
        return;
    }

    if (phrase === "") {
        console.log("addProposition empty phraseAdd");
        return;
    }

    if (to === "") {
        console.log("addProposition empty targetConceptAdd");
        return;
    }

    var fromId = getConceptId(from);
    console.log("addProposition: fromId", fromId);

    var phraseId;
    var fromPhraseConnection;
    if (fromId) {
        fromPhraseConnection = getConceptPhraseConnection(from, phrase);

        if (fromPhraseConnection) {
            //Example: lineId: "line_3" sourceId: "use_concept_1" targetId: "use_phrase_4"
            phraseId = fromPhraseConnection.targetId;
        }

    } else {
        fromId = createConceptFromText(from, 20, 20);
        console.log("addProposition: created from id: ", fromId);
    }

    var toId = getConceptId(to);
    console.log("addProposition: toId", toId);
    var phraseToConnection;
    if (toId) {
        phraseToConnection = getPhraseConceptConnection(phrase, to);
        if (phraseToConnection) {
            phraseId = phraseToConnection.sourceId;
        }
    } else {
        toId = createConceptFromText(to, 20, 20);
        console.log("addProposition: created to id: ", toId);
    }

    console.log("addProposition: fromPhraseConnection:", fromPhraseConnection);
    console.log("addProposition: phraseToConnection..:", phraseToConnection);

    const hasProposition = fromPhraseConnection && phraseToConnection;

    if (hasProposition) {
        alert("O mapa conceitual já tem esta proposição lógica.");
        return;    
    }

    if (!fromPhraseConnection) {

        if (!phraseId) {
            phraseId = createPhraseOnly(phrase);
        }

        createLineFromTo(fromId, phraseId);

        if (!phraseToConnection) {
            createLineFromTo(phraseId, toId);
        }
    } else {
        //if has a connection from source-concept to phrase, from this point it has not 
        //a connection from phrase to target-concept. So, it is necessary to create it.
        createLineFromTo(phraseId, toId);
    }

    reorgMap();
}

window.updateReport = function() {

    updateConceptsCopy();
    updateTextWords(true);
    //updateConnectionsCopy();
    updatePropositions();
    updateMetrics();
}

window.updateConceptsCopy = function() {

    var json = getJsonConcepts();
    let text = "\n";
    let set = new Set();

    for (let i = 0; i < json.length; i++) {
        
        let concept = json[i];
        let content = concept.content;
        if (!set.has(content.toUpperCase())) {
            set.add(content.toUpperCase());
            text += concept.content + ", ";        
        }
    }

    if (text.length > 1) {
        text = text.slice(0, -2);
    }

    adjustTextArea("conceptsCopy", text);
}


window.addConcept = function(useId, content) {

    var concept = new Concept();
    concept.useId = useId;
    concept.content = content;
    concept.upper = content.toUpperCase();
    //console.log("addConcept: ", concept.upper);

    var aConcepts = getArrayConcepts();
    aConcepts.push(concept);
    var sContent = JSON.stringify(aConcepts);
    document.getElementById("concepts").textContent = sContent;

    var aMapNodes = getArrayMapNodes();
    aMapNodes.push(concept.useId);
    var sMapNodes = JSON.stringify(aMapNodes);
    document.getElementById("mapNodes").textContent = sMapNodes;

    updateReport();
}

/**
 * 
 * @param {*} element A concept or a phrase 
 */
window.removeElement = function(element) {

    var json = getJsonConcepts();
    var newConnections = new Array();
    for (let i = 0; i < json.length; i++) {
        
        let concept = json[i];
        
        if (concept.useId !== element.id) {
            newConnections.push(concept);
        }
    }

    document.getElementById("concepts").textContent = JSON.stringify(newConnections);
    updateReport();
}

window.updateConcept = function(useId, content) {

    var json = getJsonConcepts();
    var newConnections = new Array();
    for (let i = 0; i < json.length; i++) {
        
        let concept = json[i];
        
        if (concept.useId === useId) {
            concept.content = content;
            concept.upper = content.toUpperCase();
            //console.log("updateConcept: concept.upper", concept.upper);
        }
        newConnections.push(concept);
    }

    document.getElementById("concepts").textContent = JSON.stringify(newConnections);
    updateReport();

    if (content !== "..") {
        if (useId === "use_concept_1") {
            let mapName = document.getElementById("mapName");
            if (null !== mapName) {
                if (mapName.value !== content) {
                    mapName.value = content;
                }
            }
        }
    }
}


window.addConnection = function(connection) {

    var line = document.getElementById(connection.lineId);
    var source = document.getElementById(connection.sourceId);
    var target = document.getElementById(connection.targetId);

    var sourceCenter = svgGetCenter(source);
    var targetCenter = svgGetCenter(target);

    line.setAttribute("x1", sourceCenter.x);
    line.setAttribute("y1", sourceCenter.y);
    line.setAttribute("x2", targetCenter.x);
    line.setAttribute("y2", targetCenter.y);

    var json = getJsonConnections();
    
    var aConnections = getArrayConnections();
    aConnections.push(connection);
    var connections = document.getElementById("connections");
    connections.textContent = JSON.stringify(aConnections);

    var aMapNodes = getArrayMapNodes();
    if (connection.sourceId.includes("use_phrase")) {
        var has = false;
        for (let i = 0; i < aMapNodes.length; i++) {
            const item = aMapNodes[i];
            if (connection.sourceId === item) {
                has = true;
                break;
            }
        }
        if (!has) {
            aMapNodes.push(connection.sourceId);
        }
    }
    var sMapNodes = JSON.stringify(aMapNodes);
    document.getElementById("mapNodes").textContent = sMapNodes;

    updateReport();
}

window.getLastConnection = function() {

    const json = getJsonConnections();

    if (json.length === 0) {
        return;
    }

    return json[json.length - 1];
}

window.removeConnectionByLineId = function(lineId) {

    var json = getJsonConnections();

    var newConnections = new Array();
    for (let i = 0; i < json.length; i++) {
        
        let connection = json[i];
        
        if (connection.lineId !== lineId) {
            newConnections.push(connection);
        }
    }

    var connections = document.getElementById("connections");
    connections.textContent = JSON.stringify(newConnections);
}

/**
 * if deleted element is a concept
 *    delete all lines where ie is a source
 * 
 * @param {*} element 
 */
window.deleteLost = function(element) {

    console.log("deleteLost: element.id", element.id);
    
    deleteMapNode(element);

    /*
    If deleted is phrase
        delete only his lines.
    If deleted is concept 
        and source
            verify if his phrase target still have sources
            If not, delete phrase target also.
        and target
            verify is his phrase source still have targets
            If not, delete phrase source also.
    */

    var connection;
    var phraseId;
    var count;
    if (element.id.includes("concept")) {

        const sources = isSourceOf(element);
        const targets = isTargetOf(element);
     
        console.log("deleteLost: sources.length", sources.length);
        if (sources.length !== 0) {
             for (let i = 0; i < sources.length; i++) {
                connection = sources[i];
                phraseId = connection.targetId;
                deleteNoMoreSourcesPhrase(phraseId);
             }
        }
     
        console.log("deleteLost: targets.length", targets.length);
        if (targets.length !== 0) {
            for (let i = 0; i < targets.length; i++) {
                connection = targets[i];
                phraseId = connection.sourceId;
                deleteNoMoreTargetsPhrase(phraseId);
            }
        }
    }

    const lines = getSourceTargetLines(element);
    deleteSourceLines(element, lines.sources);
    deleteTargetLines(element, lines.targets);

    updateReport();
}

window.deleteNoMoreSourcesPhrase = function(phraseId) {

    var json = getJsonConnections();

    var count = 0;
    for (let i = 0; i < json.length; i++) {
        
        let connection = json[i];
        
        if (connection.targetId === phraseId) {
            count++;
        }
    }

    console.log("deleteNoMoreTargetsPhrase: phraseId", phraseId);
    console.log("deleteNoMoreTargetsPhrase: count", count);
    
    if (count < 2) {
        let element = document.getElementById(phraseId);
        deleteElement(element);
    }
}

window.deleteNoMoreTargetsPhrase = function(phraseId) {

    var json = getJsonConnections();

    var count = 0;
    for (let i = 0; i < json.length; i++) {
        
        let connection = json[i];
        
        if (connection.sourceId === phraseId) {
            count++;
        }
    }

    console.log("deleteNoMoreTargetsPhrase: phraseId", phraseId);
    console.log("deleteNoMoreTargetsPhrase: count", count);
    
    if (count < 2) {
        let element = document.getElementById(phraseId);
        deleteElement(element);
    }
}

window.isSourceOf = function(element) {

    var sources = new Array();
    var json = getJsonConnections();

    for (let i = 0; i < json.length; i++) {
        
        let connection = json[i];
        
        if (connection.sourceId === element.id) {
            sources.push(connection);
        }
    }

    return sources;
}

window.isTargetOf = function(element) {

    var sources = new Array();
    var json = getJsonConnections();

    for (let i = 0; i < json.length; i++) {
        
        let connection = json[i];
        
        if (connection.targetId === element.id) {
            sources.push(connection);
        }
    }

    return sources;
}

window.updateConnectionsCopy = function() {

    let connectionsCopy = document.getElementById("connectionsCopy");
    connectionsCopy.innerHTML = connections.textContent;
}

window.getSourceTargetLines = function(element) {

    var sources = new Array();
    var targets = new Array();

    var json = getJsonConnections();
    var connection;

    for (let i = 0; i < json.length; i++) {
        
        connection = json[i];
        
        if (connection.sourceId === element.id) {
            sources.push(connection.lineId);
        }

        if (connection.targetId === element.id) {
            targets.push(connection.lineId);
        }
    }

    return {
        sources: sources,
        targets: targets
    } 
}

window.deleteSourceLines = function(element, lines) {

    console.log("deleteSourceLines: lines", lines);

    var lineId, line;

    for (let i = 0; i < lines.length; i++) {

        lineId = lines[i];
        line = document.getElementById(lineId);
        if (line !== null) {
            line.parentNode.removeChild(line);
        }
        removeConnectionByLineId(lineId);
    }
}

window.deleteTargetLines = function(element, lines) {

    console.log("deleteTargetLines: lines", lines);

    var lineId, line;

    for (let i = 0; i < lines.length; i++) {

        lineId = lines[i];
        line = document.getElementById(lineId);
        if (line !== null) {
            line.parentNode.removeChild(line);
        }
        removeConnectionByLineId(lineId);
    }
}

window.updatePropositions = function() {

    var json = getJsonConnections();
    var connection;
    var text;
    var content = "\n";
    var rows = 0;

    for (let i = 0; i < json.length; i++) {
        
        connection = json[i];
        
        if (connection.sourceId.includes("use_concept")) {

            text = getTextFromUseId(connection.sourceId);
            let sourceAndConn = text.textContent + " ";
            text = getTextFromUseId(connection.targetId);
            sourceAndConn += text.textContent + " ";
            
            let targetsTargetTexts = getTargetConcepts(connection.targetId);

            for (let i = 0; i < targetsTargetTexts.length; i++) {
                let targetsTargetText = targetsTargetTexts[i]; 
                content += sourceAndConn + targetsTargetText + "\n";
                rows++;
            }            
        }
    }

    window.metrics.numberPropositions = rows;

    if (content.length > 1) {
        content = content.slice(0, -1);
    }

    adjustTextArea("propositions", content, rows + 1);
}

window.adjustTextArea = function(textAreaId, content, rows) {

    let element = document.getElementById(textAreaId);

    if (rows !== undefined) {
        element.setAttribute("rows", rows);    
    }
 
    if (content !== undefined) {
         element.innerHTML = content;
    }
    
    element.style.height = "5px";
    element.style.height = ((element.scrollHeight)+15)+"px";
}

/**
 * Number of potential communication channels: n(n-1)/2.
 */
window.calculateComplexity = function() {

    const nodes = window.metrics.differentConcepts;

    let networkComplexity = ((nodes) * (nodes - 1)) / 2;

    window.metrics.complexity = networkComplexity + window.metrics.numberPropositions;
}

window.setTd = function(id, content) {

    let td = document.getElementById(id);
    td.innerHTML = content;
}

window.updateMetrics = function() {

    countDifferentConcepts();
    countIsolateConcepts();
    calculateComplexity();

    var content = "";

    setTd("differentConcepts",        window.metrics.differentConcepts);
    setTd("differentWords",           window.metrics.differentWords);
    setTd("isolateConcepts",          window.metrics.isolateConcepts);
    setTd("numberPropositions",       window.metrics.numberPropositions);
    setTd("complexity",               window.metrics.complexity);
}

window.getTargetConcepts = function(sourceId) {

    var aTargetConcepts = new Array();

    var json = getJsonConnections();
    var connection;
    for (let i = 0; i < json.length; i++) {
        
        connection = json[i];
        if (connection.sourceId === sourceId) {
            let text = getTextFromUseId(connection.targetId);
            if (null !== text) {
                aTargetConcepts.push(text.textContent); 
            }
        }
    }

    return aTargetConcepts;
}

window.moveLines = function(lines, x, y) {

    var lineId, line;

    var sources = lines.sources;
    for (let i = 0; i < sources.length; i++) {
        lineId = sources[i];
        line = document.getElementById(lineId);
        if (line !== null) {
            line.setAttribute("x1", x);
            line.setAttribute("y1", y);    
        }
    }

    var targets = lines.targets;
    for (let i = 0; i < targets.length; i++) {
        lineId = targets[i];
        line = document.getElementById(lineId);
        if (line !== null) {
            line.setAttribute("x2", x);
            line.setAttribute("y2", y);
        }
    }
}

window.startConnection = function(source) {

    var inEditionId;

    if (source === "concept") {
        inEditionId = document.getElementById("inEdition").textContent;
    } else if (source === "phrase") {
        inEditionId = document.getElementById("inEdition").textContent;
    } else {
        return;
    }

    var useSourceId = inEditionId; 
    var useSource = document.getElementById(useSourceId);

    setAction("connection");

    // console.log("startConnection: useSourceId", useSourceId);
    // console.log("startConnection: useSource", useSource);

    var centerSelected = svgGetCenter(useSource);

    if (null !== centerSelected) {

        var line = createLine();    
        line.setAttribute("x1", centerSelected.x);
        line.setAttribute("y1", centerSelected.y);
        line.setAttribute("x2", centerSelected.x);
        line.setAttribute("y2", centerSelected.y);
    
        document.getElementById("editingLine").textContent = line.id;
    }

}
