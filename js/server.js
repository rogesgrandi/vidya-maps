/**
 * server.js
 * JavaScript functions for 
 * PHP server-side integrations.
 */

 /**
* Transfers content from cmapd to hiddenCmapd via JQuery
*/
$(function() {    
    $('#btnSaveRemote').click(function () {
        var payload = $('#cmapd').html();
        $('#hiddenCmapd').val(payload);
    });
});

