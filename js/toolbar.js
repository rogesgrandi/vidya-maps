/*
toolbar.js

window.printPage = function()
window.showHideReport = function()
window.zoomIn = function()
window.zoomOut = function()
*/

/**
 * Botão "Imprimir"
 * Deprecated
 */
window.printPage = function() {
    
    showSession("inputSession");
    showSession("textSession");
    showSession("conceptMapSession");
    showSession("reportSession");

    window.print();
}

class PositionMapElements {
    x = 0;
    y = 0;
    elementId = null;
    element = null;
    rect = null;
}

class TreeConceptMap {
    root = null;
    children = Array();
}

var coloredNodes;
var mapLine;
var mapLines;

/*
Se não há conceitos return.
Inicia o vetor de coloridos.
Para cada conceito (iniciando naturalmente no use_concept_1)
    Monta árvore
        Se o novo (conceito ou frase de ligação) não está no array de coloridos
            Adiciona à arvore
            Colore
        Senão ignora
    Adiciona árvore ao vetor de árvores
Fim
*/
window.buildArrayTreeConceptMap = function() {

    if (null === document.getElementById("use_concept_1")) {
        return;
    }

    coloredNodes = Array();

    const concepts = getArrayConcepts();
    var trees = Array();

    for (let i = 0; i < concepts.length; i++) {
        
        const concept = concepts[i].useId;
        if (colorThisNode(concept)) {
            //console.log("buildMapTrees: concept was already colored: ", concept);
            continue;
        }
        //console.log("buildMapTrees: concept was not colored before: ", concept);

        var tree = buildMapTree(concept);
        trees.push(tree);
    }

    return trees;
}

window.buildMapTree = function(root) {

    var tree = new TreeConceptMap();
    tree.root = root;

    var children = getMapNodeChildren(root);
    for (let i = 0; i < children.length; i++) {
        var child = children[i];
        tree.children.push(buildMapTree(child));
    }

    return tree;
}

window.colorThisNode = function(node) {

    for (let i = 0; i < coloredNodes.length; i++) {
        const coloredNode = coloredNodes[i];
        if (coloredNode === node) {
            return true;
        }
    }

    coloredNodes.push(node);

    return false;
}

window.getMapNodeChildren = function(root) {

    var children = new Array();
    const connections = getJsonConnections();

    for (let i = 0; i < connections.length; i++) {
        const connection = connections[i];
        if (connection.sourceId !== root) {
            continue;
        }
        if (colorThisNode(connection.targetId)) {
            continue;
        }
        children.push(connection.targetId);
    }
 
    return children;
}

window.logTreeConceptMap = function(tree, level) {

    var text = "";
    for (let i = 0; i < level; i++) {
        text = text + "   ";
    }

    const root =  getTextFromUseId(tree.root);
    text = text + root.innerHTML;
    console.log("logTreeConceptMap: level node: ", level, text);    
    
    const children = tree.children;
    var levelText = "";
    level++;
    mapLine++;
    for (let i = 0; i < children.length; i++) {
        const child = children[i];
        logTreeConceptMap(child, level);
        const childRoot = getTextFromUseId(child.root); 
        levelText = levelText + "[" + childRoot.innerHTML + "] ";
    }

}

window.positionTreeConceptMap = function() {

    const topCenter = getTopCenterContainer();
    const lineH = 50;
    var mostLeft = topCenter.x;

    for (let i = 0; i < mapLines.length; i++) {
        const mapLine = mapLines[i];
        //console.log("positionTreeConceptMap: line level all: ", i, mapLine);
        
        if (mapLine.length === 1) {

            let pme = new PositionMapElements();
            pme.elementId = mapLine[0];
            pme.element = document.getElementById(pme.elementId);
            //console.log("positionTreeConceptMap: element: ", pme.element);

            pme.rect = pme.element.getBoundingClientRect();
            pme.x = Math.round(topCenter.x - (pme.rect.width / 2));
            pme.y = Math.round(topCenter.y) + (i * lineH);
            
            mostLeft = reorg(pme, mostLeft);
        }
        else {

            var width = 0;
            for (let j = 0; j < mapLine.length; j++) {
                
                var element = document.getElementById(mapLine[j]);
                var rect = element.getBoundingClientRect();
                width = width + rect.width;
                if (j < (mapLine.length - 1)) {
                    width = width + 20;
                }
            }

            var left = Math.round(topCenter.x - (width / 2));
            for (let j = 0; j < mapLine.length; j++) {
                
                let pme = new PositionMapElements();
                pme.elementId = mapLine[j];
                pme.element = document.getElementById(pme.elementId);
                //console.log("positionTreeConceptMap: element: ", pme.element);

                pme.rect = pme.element.getBoundingClientRect();
                pme.x = left;
                pme.y = Math.round(topCenter.y) + (i * lineH);
                mostLeft = reorg(pme, mostLeft);

                left = left + pme.rect.width + 20;
            }
        }
    }

    console.log("positionTreeConceptMap: mostLeft: ", mostLeft);

    return mostLeft;
}

window.moveRightTreeConceptMap = function(moveX) {

    for (let i = 0; i < mapLines.length; i++) {

        const mapLine = mapLines[i];

        for (let j = 0; j < mapLine.length; j++) {
            
            let pme = new PositionMapElements();
            pme.elementId = mapLine[j];
            pme.element = document.getElementById(pme.elementId);

            console.log("moveRightTreeConceptMap: element.x: ", pme.element.getAttribute("x"));

            pme.x = parseFloat(pme.element.getAttribute("x")) + moveX;
            pme.y = parseFloat(pme.element.getAttribute("y"));
            reorg(pme, moveX);
        }
    }    
}

window.addNodeToLine = function(node, line) {

    //console.log("addNodeToLine: line node", line, node);
    if (mapLines.length <= line) {
        var newLine = new Array();
        newLine.push(node);
        mapLines.push(newLine);
    } else {
        var mapLine = mapLines[line];
        mapLine.push(node);
    }
}

window.buildMapLines = function(tree, line) {

    addNodeToLine(tree.root, line);
    
    const children = tree.children;
    line++;
    for (let i = 0; i < children.length; i++) {
        const child = children[i];
        buildMapLines(child, line);
    }

    //console.log("buildMapLines: mapLines: ", mapLines);
}

window.switchAddProposition = function() {
    var session = document.getElementById("addPropositionSubSession");
    let sessionClassList = session.classList;
    
    if (sessionClassList.contains("hide")) {
        session.setAttribute("class", "show");
    } else {
        session.setAttribute("class", "hide");
    }

}

window.reorgMap = function() {

    var arrayTreeConceptMap = buildArrayTreeConceptMap();

    if (!arrayTreeConceptMap) {
        return;
    }


    //console.log("reorgMap: arrayTreeConceptMap.length: ", arrayTreeConceptMap.length);
    //console.log("reorgMap: arrayTreeConceptMap: ", arrayTreeConceptMap);
    mapLines = new Array();

    for (let i = 0; i < arrayTreeConceptMap.length; i++) {
        const treeConceptMap = arrayTreeConceptMap[i];
        buildMapLines(treeConceptMap, mapLines.length);
        let mostLeft = positionTreeConceptMap();
        if (mostLeft < 0) {
            moveRightTreeConceptMap((-mostLeft) + 20);
        } else if (mostLeft < 20) {
            moveRightTreeConceptMap(20 - mostLeft);
        }
        //logTreeConceptMap(treeConceptMap, 0);
    }

    //updateConnectionsCopy(); //for debugging.

    setMinSize();
}

window.reorg = function(pme, mostLeft) {
    
    pme.element = document.getElementById(pme.elementId);
    moveElement(pme.element, pme.x, pme.y);

    if (pme.x < mostLeft) {
        mostLeft = pme.x;
    }
    return mostLeft;
}


/**
 * Botão Aumentar Zoom
 */
window.zoomIn = function() {
    
    var body = document.getElementsByTagName("body")[0];
    var clazz = body.getAttribute("class");
    
    switch (clazz) {
        case "body10":
            body.setAttribute("class", "body11");
            break;
        case "body11":
            body.setAttribute("class", "body12");
            break;
        case "body12":
            body.setAttribute("class", "body13");
            break;
        case "body13":
            body.setAttribute("class", "body14");
            break;
        case "body14":
            body.setAttribute("class", "body15");
            break;
    }
}

/**
 * Botão "Diminuir Zoom"
 */
window.zoomOut = function() {

    var body = document.getElementsByTagName("body")[0];
    var clazz = body.getAttribute("class");

    switch (clazz) {
        case "body15":
            body.setAttribute("class", "body14");
            break;
        case "body14":
            body.setAttribute("class", "body13");
            break;
        case "body13":
            body.setAttribute("class", "body12");
            break;
        case "body12":
            body.setAttribute("class", "body11");
            break;
        case "body11":
            body.setAttribute("class", "body10");
            break;
    }
}

/**
 * Botão "Relatório"
 */
window.showHideReport = function() {

    var btn = document.getElementById("btnReportSession");
    var session = document.getElementById("reportSession")

    if (btn.classList.contains("hide")) {
        btn.setAttribute("class", "accordion");
        session.setAttribute("class", "session-show");
        updateReport();
    } else {
        btn.setAttribute("class", "hide");
        session.setAttribute("class", "hide");
    }
}


