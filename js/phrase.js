/*
phrase.js

window.displaySpanPhrase = function(id)
window.finishPhraseEdition = function()
window.hideSpanPhrase = function(id)
window.setLineThick = function(evt)
window.setLineThin = function(evt)
*/

window.setLineDanger = function(evt) {
    evt.target.setAttribute("stroke", "red");
    evt.target.setAttribute("stroke-width", "4");
}

window.setLineThick = function(evt) {
    evt.target.setAttribute("stroke", "orange");
    evt.target.setAttribute("stroke-width", "4");
}

window.setLineThin = function(evt) {
    evt.target.setAttribute("stroke", "black");
    evt.target.setAttribute("stroke-width", "2");
}

window.lineClick = function(evt) {
    
    let go = window.confirm("Excluir conexão?");
    if (go) {
        console.log("lineClick: will exclude ", evt.target.id);
        deleteElement(evt.target);
    }
}

window.createLine = function() {

    var lineId = "line_" + getNextSequence();
    var line = document.createElementNS(svgNS, "line");
    line.setAttribute("id", lineId);
    line.setAttribute("opacity", "0.3");
    line.setAttribute("z-index", "10");
    line.setAttribute("stroke", "black");
    line.setAttribute("stroke-width", "2");
    line.setAttribute("marker-end", "url(#arrowhead)");
    line.setAttribute("onmouseenter", "setLineThick(event);");
    line.setAttribute("onmouseleave", "setLineThin(event);");
    line.setAttribute("onmousedown", "setLineDanger(event);");
    line.setAttribute("onclick", "lineClick(event);");

    var cmapd = document.getElementById("cmapd");
    cmapd.appendChild(line);

    return line;
}

window.createLineFromTo = function(fromId, toId) {

    var line = createLine();

    var connection = new Connection();
    connection.sourceId = fromId;
    connection.targetId = toId;
    connection.lineId = line.id;

    if (fromId.includes("phrase") || toId.includes("phrase")) {
        addConnection(connection);
    } else {
        createPhrase(connection);
    }

    return line;
}

window.createPhraseAndLine = function(conceptId, phrase) {

    var line = createLine();

    var connection = new Connection();
    connection.sourceId = fromId;
    connection.targetId = toId;
    connection.lineId = line.id;

    if (fromId.includes("phrase") || toId.includes("phrase")) {
        addConnection(connection);
    } else {
        createPhrase(connection);
    }

    return lineId;
}

window.createPhraseOnly = function(phrase) {

    displayForeign("foreignEditPhrase", 20, 20);
    displayDivEdit("divEditPhrase");

    document.getElementById("divEditPhrase").innerHTML = phrase;

    var phraseId = "use_phrase_" + getNextSequence();
    setInEditionId(phraseId);

    document.getElementById("newPhrase").textContent = phraseId;

    divToSvgPhrase();

    return phraseId;
}

window.createPhrase = function(connection) {

    var initialContent = document.getElementById("initialContent");
    var contentDim = getDimensions(initialContent);
    var source = document.getElementById(connection.sourceId);
    var target = document.getElementById(connection.targetId);
    var centerSource = svgGetCenter(source);
    var centerTarget = svgGetCenter(target);
    var middleX = getMiddle(centerSource.x, centerTarget.x);
    var middleY = getMiddle(centerSource.y, centerTarget.y);

    var x = middleX - Math.round(contentDim.width / 2);
    var y = middleY - Math.round(contentDim.height / 2);

    displayForeign("foreignEditPhrase", x, y);
    hideSpanPhrase("spanPhrase1");
    hideSpanPhrase("spanPhrase2");
    displayDivEdit("divEditPhrase");

    var phraseId = "use_phrase_" + getNextSequence();
    setInEditionId(phraseId);

    document.getElementById("newPhrase").textContent = phraseId;

    phraseUseId = divToSvgPhrase();

    var line = document.getElementById(connection.lineId);

    var connection1 = new Connection();
    connection1.sourceId = source.id;
    connection1.targetId = phraseUseId;
    connection1.lineId = line.id;
    addConnection(connection1);

    var line2 = createLine();
    var connection2 = new Connection();
    connection2.sourceId = phraseUseId;
    connection2.targetId = target.id;
    connection2.lineId = line2.id;
    addConnection(connection2);

    console.log("connected two concepts: phraseId:", phraseId);
    // var inMovement = document.getElementById("inMovement");
    // inMovement.textContent = phraseId;
    // var text = getTextFromUseId(phraseId);
    // editPhrase(text);

    return phraseUseId;
}

window.displaySpanPhrase = function(id) {
    var spanLink = document.getElementById(id);
    spanLink.style.display = "inline-block";
}

window.divEditPhraseKeyPress = function(evt) {

    if (evt.code === "Enter") {
        document.getElementById("divEditPhrase").blur();
        divToSvgPhrase();
    }
}

window.divEditPhraseFocus = function() {
    selectAllText("divEditPhrase");
}

window.divToSvgPhrase = function(escape) {

    if (escape === undefined) {
        escape = false;
    }

    var box, width, height;
    var defs, g, use, rect, text;

    var browser = getBrowser();
 
    var cmapd          = document.getElementById("cmapd");
    var divEditPhrase  = document.getElementById("divEditPhrase");
    var newPhrase      = document.getElementById("newPhrase");

    var inEditionId = getInEditionId();
    var id = inEditionId.substring(4); //use_phrase_4 => phrase_4

    var content = divEditPhrase.innerHTML;
    content = content.replaceAll("&nbsp;", "");
    content = content.replace(/<br\s*\/?>/gi,' ');
    content = content.trim();
    divEditPhrase.innerHTML = content;
   
    var rectId = "rect_" + id;
    var textId = "text_" + id;
    var useId  = "use_" + id;

    if (newPhrase.textContent === "") {

        // do not allow empy existing phrase
        if (content.trim() === "") {
            content = ".....";
        }

        rect = document.getElementById(rectId);
        rect.setAttribute("class", "textRectConn");
        text = document.getElementById(textId);
        use  = document.getElementById(useId);
        use.setAttribute('class', "draggable");

    } else {

        // do not create a new empty phrase
        if (content.trim() === "") {
            finishPhraseEdition();
            return useId;
        }

        //create a new phrase
        defs = document.createElementNS(svgNS, "defs");
        g    = document.createElementNS(svgNS, "g");
        use  = document.createElementNS(svgNS, 'use');
    
        g.setAttribute("id", id);
        use.setAttribute("id", useId);
        use.setAttributeNS(useNS, "xlink:href", "#" + id);
        use.setAttribute('class', "draggable");

        rect = createRect(rectId, "phrase"); 
        text = createText(textId, "phrase");
     
        g.appendChild(rect);
        g.appendChild(text);
        defs.appendChild(g);
        cmapd.appendChild(defs);
        cmapd.appendChild(use);
    }

    if (!escape) {
        text.textContent = content;
    }

    var pos = getEditingPosition("divEditPhrase");

    if (browser === "Firefox") {
        text.setAttribute("dominant-baseline", "middle");
        text.setAttribute("alignment-baseline", "center");
        box = divEditPhrase.getBoundingClientRect();

        width = box.width + 2;
        height = box.height - 5;
    } else {
        //var box = text.getBBox(); //also ok
        box = text.getBoundingClientRect();
        width = box.width + 10;
        height = box.height + 4;
    }

    rect.setAttribute("width",  width);
    rect.setAttribute("height", height);
   
    use.setAttribute("x", pos.x);
    use.setAttribute("y", pos.y);

    adjustLines(use);

    finishPhraseEdition();

    return useId;
}

window.editPhrase = function(text) {

    console.log("editPhrase: text", text);

    var textId = text.id;
    var shapeId = textId.substring(5);
    var useId  = "use_" + shapeId;
    
    var inMovement = document.getElementById("inMovement");
    var inMovementId = inMovement.textContent;
    var inEditionId = getInEditionId();

    console.log("editPhrase inEditionId useId:", inEditionId, useId);
    if ((inEditionId === "") && (inMovementId !== useId)) {
        setInMovementId(useId);
        return;
    } else {
        clearInMovementId();
    }
    var use  = document.getElementById(useId);
    var text = document.getElementById(textId);
    var bBox = use.getBBox();

    use.setAttribute('class', "invisible");

    displayForeign("foreignEditPhrase", bBox.x, bBox.y);
    displaySpanPhrase("spanPhrase1");
    displaySpanPhrase("spanPhrase2");
    
    var divEdit = displayDivEdit("divEditPhrase");
    divEdit.innerHTML = text.textContent;

    console.log("editPhrase: setInEditionId", useId);
    setInEditionId(useId);
}

window.finishPhraseEdition = function() {

    hideForeign("foreignEditPhrase");
    hideDiv("divEditPhrase");

    document.getElementById("inEdition").textContent = "";
    document.getElementById("newPhrase").textContent = "";
    
    updateReport();

    setMinSize();

    setAction("move");
}

window.hideSpanPhrase = function(id) {
    var spanLink = document.getElementById(id);
    spanLink.style.display = "none";
}

