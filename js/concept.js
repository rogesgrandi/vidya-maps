/*
concept.js

window.divEditConceptKeyPress = function(evt)
window.editConcept = function(text)
*/

window.getNextUseConceptId = function() {

    var mapNodes = document.getElementById("mapNodes").textContent;

    if (!mapNodes.includes("use_concept_1")) {
        return "use_concept_1"; 
    } 

    return "use_concept_" + getNextSequence(); 
}

window.createConcept = function(text, x, y) {

    //console.log("createConcept: x y", x, y);

    var mapNodes = document.getElementById("mapNodes").textContent;
    
    var seq = getNextSequence();
    var useId;
    if (mapNodes.includes("use_concept_1")) {
        useId = "use_concept_" + seq; 
    } else {
        useId = "use_concept_1"; 
    }

    displayForeign("foreignEditConcept", x, y);
    hideSpanPhrase("spanConcept1");
    hideSpanPhrase("spanConcept2");
    var divEdit = displayDivEdit("divEditConcept");
    if (text) {
        divEdit.innerHTML = text.trim();
    }
    
    if (useId === "use_concept_1") {
        let mapName = document.getElementById("mapName");
        if (mapName.value.trim() !== "") {
            divEdit.innerHTML = mapName.value.trim();
        }
    }
    setInEditionId(useId);
    document.getElementById("newConcept").textContent = useId; 

    console.log("createConcept: divEdit.innerHTML", divEdit.innerHTML);

    addConcept(useId, divEdit.innerHTML);
    finishEditions();
    setInMovementId(useId);

    return useId;
}    

window.createConceptFromText = function(text, x, y) {
    return createConcept(text, x, y);
}

window.createConceptFromMouse = function(evt) {

    var leftTop = getMouseCoord(evt);

    return createConcept(null, leftTop.x, leftTop.y);
}

window.divEditConceptFocus = function() {
    selectAllText("divEditConcept");
}

window.divEditConceptKeyPress = function(evt) {

    if (evt.code === "Enter") {
        document.getElementById("divEditConcept").blur();
        divToSvgConcept();
    }
}

window.divToSvgConcept = function(escape) {

    var box, width, height;
    var defs, g, use, rect, text;

    if (escape === undefined) {
        escape = false;
    }

    const browser = getBrowser();
 
    var divEditConcept = document.getElementById("divEditConcept");
    var inEditionId    = getInEditionId();
    var newConcept     = document.getElementById("newConcept");
    
    var content = divEditConcept.innerHTML;
    content = content.replaceAll("&nbsp;", "");
    content = content.replace(/<br\s*\/?>/gi,' ');
    content = content.trim();
    divEditConcept.innerHTML = content;

    var id = inEditionId.substring(4); //use_concept_1 => concept_1
    var rectId = "rect_" + id;
    var textId = "text_" + id;
    var useId  = "use_" + id;

    if (newConcept.textContent === "") {

        // do not allow empy existing concept
        if (content.trim() === "") {
            content = ".....";
        }

        rect = document.getElementById(rectId);
        if (rect === null) {
            return;
        }
        if (rectId == "rect_concept_1") {
            rect.setAttribute("class", "textRectMain");
        } else {
            rect.setAttribute("class", "textRect");
        }
        text = document.getElementById(textId);
        use  = document.getElementById(useId);
        use.setAttribute('class', "draggable");

    } else {

        // do not create a new empty concept
        if (content.trim() === "") {
            finishConceptEdition();
            return;
        }

        rect = createRect(rectId, "concept");
        text = createText(textId, "concept");
        use = createUse(id, rect, text);

        if (use.id === "use_concept_1") {
            rect.setAttribute("class", "textRectMain");
            //console.log("divToSvgConcept: rect", rect);
        }        

    }

    if (!escape) {
        text.textContent = content;
        updateConcept(useId, text.textContent);
    }

    var pos = getEditingPosition("divEditConcept");

    if (browser === "Firefox") {
        text.setAttribute("dominant-baseline", "middle");
        text.setAttribute("alignment-baseline", "center");
        box = divEditConcept.getBoundingClientRect();
        width = box.width + 2;
        height = box.height - 5;
    } else {
        //var box = text.getBBox(); //also ok
        box = text.getBoundingClientRect();
        width = box.width + 16;
        height = box.height + 4;
    }

    rect.setAttribute("width",  width);
    rect.setAttribute("height", height);
   
    use.setAttribute("x", pos.x);
    use.setAttribute("y", pos.y);

    adjustLines(use);

    finishConceptEdition();
}

window.editConcept = function(text) {

    var inMovement = document.getElementById("inMovement");
    var inMovementId = inMovement.textContent;
    var inEditionId = getInEditionId();
    var shapeId = text.id.substring(5);
    var useId  = "use_" + shapeId;

    if ((inEditionId === "") && (inMovementId !== useId)) {
        setInMovementId(useId);
        return;
    } else {
        clearInMovementId();
    }

    var use  = document.getElementById(useId);
    var content = text.textContent;
    var bBox = use.getBBox();

    use.setAttribute('class', "invisible"); 

    displayForeign("foreignEditConcept", bBox.x, bBox.y);

    spanConcept1.style.display = "inline-block";
    spanConcept2.style.display = "inline-block";
    inMovement.textContent = useId;
    
    var divEditConcept = document.getElementById("divEditConcept");

    if (useId == "use_concept_1") {
        divEditConcept.setAttribute("class", "insideforeignConceptMain");
    } else {
        divEditConcept.setAttribute("class", "insideforeignConcept");
    }

    divEditConcept.style.display = "inline-block";
    divEditConcept.innerHTML = content;
    divEditConcept.focus();        

    //console.log("editConcept: setInEditionId", id);
    setInEditionId(useId);
}

window.finishConceptEdition = function() {

    hideForeign("foreignEditConcept");
    hideDiv("divEditConcept");

    document.getElementById("inEdition").textContent = "";
    document.getElementById("newConcept").textContent = "";

    updateReport();

    setMinSize();

    setAction("move");
}

window.updateMainConcept = function() {

    var mapName = document.getElementById("mapName");
    var mapNameContent = mapName.value.trim();
    var mainConcept = document.getElementById("use_concept_1");
    var mainConceptContent = "";
    var text = null;

    //if main concept already created, gets its content.
    if (null !== mainConcept) {
        text = getTextFromUseId(mainConcept.id);
        if (null !== text) {
            mainConceptContent = text.textContent;
        }
    }        

    //if map name empty, tries to get main concept content and return
    if (mapNameContent === "") {
        if (mainConceptContent !== ".....") {
            mapName.value = mainConceptContent;
        }
        return;
    }

    //if map name is not empty 
    console.log("updateMainConcept: mainConcept", mainConcept);
     
    if (null === mainConcept) {
        let leftTop = getTopCenterContainer();
        createConceptFromText(mainConceptContent, leftTop.x, leftTop.y);
        finishEditions();
        return;
    } 
    
    
    if (null !== text) {
        editConcept(text);
        text.textContent = mapNameContent;
        finishEditions();
        let dim = getSvgDimensions(text);
        //console.log("updateMainConcept: dim", dim);
        let rect = getRectFromUseId(mainConcept.id);
        rect.setAttribute("width", dim.width);
        adjustLines(mainConcept);
    }
    
}



