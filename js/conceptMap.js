/*
conceptMap.js

$(document).ready(function()

window.changeContext = function()
window.addEventListener("keydown", function (event)

window.showSession = function(id)
window.hideShowSession = function(sessionId, buttonId)

window.clearInMovementId = function()
window.setInMovementId = function(id)

window.setInEditionId = function(id)
window.clearInEditionId = function()
window.getInEditionId = function()
window.getEditingPosition = function(id)
window.finishEditions = function(escape)

window.createRect = function(rectId, typeObj)
window.getRectFromUseId = function(useId)

window.setMoveClass = function(shapeId)
window.setDefaultClass = function(useId)
window.setConnectionClass = function(id)
window.setDeleteClass = function(id)

window.setAction = function(action)
window.getNextSequence = function()
window.deleteElement = function(element)

window.createUse = function(id, rect, text)

window.getTextFromUseId = function(useId)
window.createText = function(textId, typeObj)

window.displayForeign = function(id, x, y)
window.hideForeign = function(id)

window.hideDiv = function(id)

window.getMousePosition = function(evt)
window.svgMouse = function(evt)

window.getMinSizeDefaut = function()
window.setMinSize = function()
*/

var svgNS = "http://www.w3.org/2000/svg";
var useNS = "http://www.w3.org/1999/xlink";

/**
 * The concept map context may be General or Specific.
 */
window.changeContext = function() {
    
    let checked = document.querySelector('input[name="mapContext"]:checked').value;
    let specificContext = document.getElementById("specificContext");

    if (checked === "general") {
        specificContext.setAttribute("class", "hide");
    } else if (checked === "specific") {
        specificContext.setAttribute("class", "input-top-context");
    }
}

$(document).ready(function() {
    
    checkDevice();
    changeContext();
    updateReport();

    let resizeObserver = new ResizeObserver(entries => {
    });

    const divSvg = document.getElementById("divSvg");
    resizeObserver.observe(divSvg);

    const mapName = document.getElementById("mapName");
    mapName.addEventListener('blur', (event) => {
        mapName.checkValidity();
        updateMainConcept();
    });

});

window.checkDevice = function() {

    const browser = getBrowser();
    switch (browser) {
        case "Chrome":
        case "Firefox":
        case "Opera":
        break;
        default:
            var msgBrowser = document.getElementById("msgBrowser");
            msgBrowser.setAttribute("class", "msgError");
    }

    if (mobileAndTabletCheck()) {
        var msgDevice = document.getElementById("msgDevice");
        msgDevice.setAttribute("class", "msgError");
    }
}

window.addEventListener("keydown", function (event) {

    if (event.defaultPrevented) {
        return; // Do nothing if the event was already processed
    }

    var inMovementId = document.getElementById("inMovement").textContent;

    if (inMovementId === "") {
        return;
    }

    var inEditionId = document.getElementById("inEdition").textContent;

    var element = document.getElementById(inMovementId);
    var box = element.getBBox();

    switch (event.key) {
      case "Down": // IE/Edge specific value
      case "ArrowDown":
        if (!isEmpty(box)) {
            moveElement(element, box.x, (box.y + 1));
        }
        break;
      case "Up": // IE/Edge specific value
      case "ArrowUp":
        if (!isEmpty(box)) {
            moveElement(element, box.x, (box.y - 1));
        }
        break;
      case "Left": // IE/Edge specific value
      case "ArrowLeft":
        if (!isEmpty(box)) {
            moveElement(element, (box.x - 1), box.y);
        }
        break;
      case "Right": // IE/Edge specific value
      case "ArrowRight":
        if (!isEmpty(box)) {
            moveElement(element, (box.x + 1), box.y);
        }
        break;
      case "F2":
      case "Enter":
        if ((inMovementId !== "") && (inEditionId === "")) {
            if (inMovementId.includes("use_concept")) {
                //setInEditionId(inMovementId);
                let text = getTextFromUseId(inMovementId);
                editConcept(text);
            } else if (inMovementId.includes("use_phrase")){
                //setInEditionId(inMovementId);
                let text = getTextFromUseId(inMovementId);
                editPhrase(text);
            }
        } else {
            finishEditions();
        }
        break;
      case "Esc": // IE/Edge specific value
      case "Escape":
        finishEditions(true);
        break;
      case "Delete":
        deleteElement(element);
        break;
      default:
        return; // Quit when this doesn't handle the key event.
    }
  
    // Cancel the default action to avoid it being handled twice
    event.preventDefault();
  }, true);


window.moveElement = function(element, x, y) {

    element.setAttribute("x", x);
    element.setAttribute("y", y);

    const center = svgGetCenter(element);
    const lines = getSourceTargetLines(element);
    moveLines(lines, center.x, center.y);
}

window.showSession = function(id) {

    let element = document.getElementById(id)
    if ("hide" === element.getAttribute("class")) {
        element.setAttribute("class", "session-show");
    }
}

window.hideShowSession = function(sessionId, buttonId) {

    let session = document.getElementById(sessionId);
    let button = document.getElementById(buttonId); 

    if (session && button) {
        
        let btnClassName = button.className;
        let sessionClassList = session.classList;
    
        if (sessionClassList.contains("session-show")) {
            session.setAttribute("class", "hide");
        } else {
            session.setAttribute("class", "session-show");
        }

        if (sessionId == "reportSession") {
            adjustTextArea("conceptsCopy");
            adjustTextArea("propositions");
        }

        if (btnClassName === "accordion") {
            button.className = "accordion-close";
        } else {
            button.className = "accordion";
        }
    } 
}

window.finishEditions = function(escape) {

    clearInMovementId();

    let inEditionId = getInEditionId();

    if (inEditionId.includes("concept")) {
        divToSvgConcept(escape);
    } else if (inEditionId.includes("phrase")) {
        divToSvgPhrase(escape);
    }

    updateReport();
}

window.trashMouseDown = function() {
    setAction("delete");
    setClass("trash", "trashDrag");
    finishEditions();
}

window.resetTrash = function() {
    let trash = document.getElementById("trash");
    trash.setAttribute("x", 3);
    trash.setAttribute("y", 25);
    //trash.setAttribute("class", "trash");
}

window.clearInMovementId = function() {

    let inMovement = document.getElementById("inMovement");
    if (!isEmpty(inMovement.textContent)) {
        setDefaultClass(inMovement.textContent);
        inMovement.textContent = "";
    }
}

window.clearInEditionId = function() {

    let inEdition = document.getElementById("inEdition");
    if (!isEmpty(inEdition.textContent)) {
        let use = document.getElementById(inEdition.textContent);
        inEdition.textContent = "";
    }
}

window.setMoveClass = function(shapeId) {

    finishEditions();

    if (isEmpty(shapeId)) {
        //console.log("setMoveClass: empty");
        return;
    }

    let useId = "use_" + shapeId;
    let use = document.getElementById(useId);
    use.setAttribute('class', "useMove"); 

    document.getElementById("inMovement").textContent = useId;
}

window.setDefaultClass = function(useId) {

    if (isEmpty(useId)) {
        return;
    }

    var target;

    if (useId.includes("concept")) {

        target = getRectFromUseId(useId); 
    
        if (target !== null) {
            if (useId === "use_concept_1") {
                target.setAttribute("class", "textRectMain");
            } else {
                target.setAttribute("class", "textRect");
            }
        }

    } else if (useId.includes("phrase")) {

        target = getRectFromUseId(useId); 
    
        if (target !== null) {
            target.setAttribute("class", "textRectConn");
        }

    } else if (useId.includes("line")) {

        target = document.getElementById(useId);

        if (target !== null) {
            target.setAttribute("stroke", "black");
            target.setAttribute("stroke-width", "2");
        }
    }
}

window.setConnectionClass = function(id) {

    if (isEmpty(id)) {
        return;
    }

    var target;

    if ((id.includes("concept")) ||
        (id.includes("phrase"))) {

        id = "rect" + id.substring(3); //use_concept_n or use_phrase_n
        target = document.getElementById(id);
    
        if (target === null) {
            return;
        }

        target.setAttribute("class", "textRectConnection");
    }
}

window.setDeleteClass = function(id) {

    if (isEmpty(id)) {
        return;
    }

    var target;

    if ((id.includes("concept")) ||
        (id.includes("phrase"))) {

        id = "rect" + id.substring(3); //use_concept_n or use_phrase_n
        target = document.getElementById(id);
    
        if (target === null) {
            return;
        }

        target.setAttribute("class", "textRectDanger");

    } else if (id.includes("line")) {

        target = document.getElementById(id);

        if (target === null) {
            return;
        }
    
        target.setAttribute("stroke", "red");
        target.setAttribute("stroke-width", "4");   
    }
}

window.hideLogoShowTrash = function() {
    document.getElementById("logoIn").setAttribute("class", "invisible");
    //document.getElementById("trash").setAttribute("class", "trash");
}

window.setAction = function(action) {
    document.getElementById("action").textContent = action;
}

window.getNextSequence = function() {

    let sequence = document.getElementById("sequence");
    const next = parseInt(sequence.textContent) + 1;
    sequence.textContent = next;

    return next;
}

window.deleteElement = function(element) {

    element.parentNode.removeChild(element);
    removeElement(element); 
    deleteLost(element);
    updateReport();
    clearInEditionId();
    clearInMovementId();
}

window.createUse = function(id, rect, text) {
  
    let g = document.createElementNS(svgNS, "g");
    g.setAttribute("id", id);
    g.appendChild(rect);
    g.appendChild(text);

    let use = document.createElementNS(svgNS, 'use');
    use.setAttribute("id", "use_" + id);
    use.setAttributeNS(useNS, "xlink:href", "#" + id);
    use.setAttribute('class', "draggable");

    let defs = document.createElementNS(svgNS, "defs");
    defs.appendChild(g);

    let cmapd = document.getElementById("cmapd");
    cmapd.appendChild(defs);
    cmapd.appendChild(use);

    return use;
}

window.getEditingPosition = function(id) {

    var divEdit = document.getElementById(id);
    var cmapd = document.getElementById("cmapd");
    var rectCmapd = cmapd.getBoundingClientRect();
    var rectText  = divEdit.getBoundingClientRect();
    var x, y;

    x = (rectText.left - rectCmapd.left).toFixed(0);
    y = (rectText.top  - rectCmapd.top).toFixed(0);
    
    return {
        x: x,
        y, y
    }
}

window.setInEditionId = function(id) {

    if (id === undefined) {
        document.getElementById("inEdition").textContent = "";
    } else {
        document.getElementById("inEdition").textContent = id;
    }
}

window.getTextFromUseId = function(useId) {

    if (useId.includes("use_")) {
        let textId = "text_" + useId.substring(4);
        return document.getElementById(textId);
    }

    return null;
}

window.getRectFromUseId = function(useId) {

    if (!useId) {
        return;
    }

    if (useId.includes("use_")) {
        let textId = "rect_" + useId.substring(4);
        return document.getElementById(textId);
    }

    return;
}

window.setInMovementId = function(id) {

    let inMovement = document.getElementById("inMovement");
    let inMovementId = inMovement.textContent;

    setDefaultClass(inMovementId);

    if (isEmpty(id)) {
        console.log("setInMovementId: id empty");    
        document.getElementById("inMovement").textContent = "";
    } else {
        document.getElementById("inMovement").textContent = id;

        if (id.includes("use_")) {
            let rectId = "rect_" + id.substring(4);
            element = document.getElementById(rectId);
            if (null !== element) {
                if (rectId.includes("concept")) {
                    if (rectId === "rect_concept_1") {
                        element.setAttribute("class", "textRectMainSelected");
                    } else {
                        element.setAttribute("class", "textRectSelected");
                    }
                } else if (rectId.includes("phrase")) {
                    element.setAttribute("class", "textRectSelectedConn");
                }
            }
        }
    }
}

window.displayForeign = function(id, x, y) {
    let foreign = document.getElementById(id);
    foreign.setAttribute("width", "100%");
    foreign.setAttribute("height", "100%");
    foreign.setAttribute("x", x);
    foreign.setAttribute("y", y);
    
    foreign.style.display = "inline-block";
}

window.hideForeign = function(id) {
    let foreign = document.getElementById(id);
    foreign.style.display = "none";
    foreign.width = "0";
    foreign.height = "0";
}

window.hideDiv = function(id) {
    var div = document.getElementById(id);
    div.style.display = "none";
    div.innerHTML = "";
}

window.displayDivEdit = function(id) {
    var divEdit = document.getElementById(id);
    divEdit.style.display = "inline-block";
    divEdit.innerHTML = ".....";
    divEdit.focus();

    return divEdit;
}

window.createRect = function(rectId, typeObj) {

    let rect = document.createElementNS(svgNS, "rect");
    rect.setAttribute("id", rectId);
    rect.setAttribute("x", "0");
    rect.setAttribute("y", "0");
    rect.setAttribute("rx", "1");
    rect.setAttribute("ry", "1");

    if (typeObj === "concept") {
        rect.setAttribute("class", "textRect");
    } else if (typeObj === "phrase") {
        rect.setAttribute("class", "textRectConn");
    }

    return rect;
}

window.createText = function(textId, typeObj) {
   
    let text = document.createElementNS(svgNS, "text");
    text.setAttribute("id", textId);
    text.setAttribute("x", "5");
    text.setAttribute("y", "15");
    text.setAttribute("z-index", "99");

    if (typeObj === "concept") {
        text.setAttribute("onclick", "editConcept(" + textId +");");
    } else if (typeObj === "phrase") {
        text.setAttribute("onclick", "editPhrase(" + textId +");");
    }

    return text;
}

window.getInEditionId = function() {  
    return document.getElementById("inEdition").textContent;
}

window.getTopCenterContainer = function() {

    let element = document.getElementById("cmapd");
    let box = element.getBoundingClientRect();

    let x = (box.x + (box.width / 2));
    let y = 20;

    return {
        x: x,
        y: y
    }
}

window.getMouseCoord = function(evt) {
    
    const cmapd = document.getElementById("cmapd");
    var pt = cmapd.createSVGPoint();
    pt.x = evt.clientX;
    pt.y = evt.clientY;
    var localpoint = pt.matrixTransform(cmapd.getScreenCTM().inverse());
    localpoint.x = Math.round(localpoint.x);
    localpoint.y = Math.round(localpoint.y);

    return localpoint;
};

window.getMousePosition = function(evt) {

    const cmapd = document.getElementById("cmapd");
    var ctm = cmapd.getScreenCTM();
    if (evt.touches) { evt = evt.touches[0]; }
    return {
        x: (evt.clientX - ctm.e) / ctm.a,
        y: (evt.clientY - ctm.f) / ctm.d
    };
}

window.getMinSizeDefaut = function() {

    var x = 80;
    var y = 50;

    return {x, y};
}

window.setMinSize = function() {

    var min = getMinSizeDefaut();

    var array = getArrayMapNodes();
    for (let i = 0; i < array.length; i++) {
        const id = array[i];
        element = document.getElementById(id);
        min = calcMinSize(element, min);
    }

    //console.log("setMinSize: min", min);
    var divSvg = document.getElementById('divSvg');
    divSvg.style.minWidth  = min.x + "px";
    divSvg.style.minHeight = min.y + "px";
}

window.resizeMap = function(element, coord) {

    var id = element.id;

    if (id.includes("concept") ||
        id.includes("phrase")) {

        var max = getMaxPointWithCoord(element, coord);

        var divSvg = document.getElementById('divSvg');

        var rect = divSvg.getBoundingClientRect();

        if (max.x > rect.width) {
            divSvg.style.width = max.x + "px";
        }

        if (max.y > rect.height) {
            divSvg.style.height = max.y + "px";
        }

    }
}


/**
* http://www.petercollingridge.co.uk/tutorials/svg/interactive/dragging/
* https://developer.mozilla.org/pt-BR/docs/Web/SVG
* https://dev.w3.org/SVG/tools/svgweb/docs/UserManual.html
*/
window.svgMouse = function(evt) {

    var selectedElement;
    var cmapd = evt.target;
    mouseDownOnElement = false;
    container = document.getElementById("cmapd");

    cmapd.addEventListener('mousedown', svgMouseDown);
    cmapd.addEventListener('mousemove', drag);
    cmapd.addEventListener('mouseup', endDrag);
    cmapd.addEventListener('mouseleave', endDrag);
    //mobile
    cmapd.addEventListener('touchstart', svgMouseDown);
    cmapd.addEventListener('touchmove', drag);
    cmapd.addEventListener('touchend', endDrag);
    cmapd.addEventListener('touchleave', endDrag);
    cmapd.addEventListener('touchcancel', endDrag);

    function svgMouseDown(evt) {

        //console.log("svgMouseDown: evt.type", evt.type);

        let time = timeBetweenClicks();

        //return;

        selectedElement = evt.target;
        id = selectedElement.id;

        var action = document.getElementById("action").textContent;
        var inMovement = document.getElementById("inMovement");
        var inMovementId = inMovement.textContent;
        var inEdition = document.getElementById("inEdition");
        var inEditionId = inEdition.textContent;

        if (action === "") {
            setAction("move");
            action = "move";
        }

        //console.log("svgMouseDown: inEdition id", inEditionId);    
        //console.log("svgMouseDown: click id", id);    

        if ((id.includes("concept") || id.includes("phrase")) &&
            (inEditionId.includes("concept") || inEditionId.includes("phrase")) &&
            id !== inEditionId) {

            //console.log("svgMouseDown: to link from", inEditionId);    
            //console.log("svgMouseDown: to link to", id);
            finishEditions();
            createLineFromTo(inEditionId, id);    
            return;
        }

        if (time <= 200) {
            //dblclick
            //console.log("svgMouseDown: will evaluate double click");

            if ((inMovementId !== "") &&
                (inEditionId === "") &&
                (id = "cmap")) {

                //console.log("svgMouseDown: double click while creating a concept.");

                if (inMovementId.startsWith("use_concept")) {
                    textId = "text_" + inMovementId.substring(4);
                    text = document.getElementById(textId);
                    if (null !== text) {
                        editConcept(text);
                    }
                }
                return;
            }
        }

        if (id.includes("foreign")) {
            //console.log("svgMouseDown: foreign");
            finishEditions();
            return;
        } 
        
        if (id.includes("phrase")) {

            //console.log("mouseDownDiagram: required to edit a phrase.");

        } else if ((id === "cmapd") || (id === "foreignEditConcept")) {

        
            if (inEdition.textContent === "") {
                createConceptFromMouse(evt);
            } else {
                divToSvgConcept();
            }
        }
    }
    
    function drag(evt) {

        if (selectedElement) {

            evt.preventDefault();

            var coord = getMousePosition(evt);
            var centerSelected = svgGetCenter(selectedElement);
            var action = document.getElementById("action");
            var targetBefore = document.getElementById("targetBefore");
            var targetBeforeId = targetBefore.textContent;
            var targetId = evt.target.id;

            //console.log("drag: targetBeforeId", targetBeforeId);

            if (id === "trash") {
    
                setDeleteClass(targetId);

                if (targetBeforeId !== targetId) {
                    setDefaultClass(targetBeforeId);
                }
            }

            if (action.textContent === "connection") {

                setConnectionClass(targetId);

                if (targetBeforeId !== targetId) {
                    setDefaultClass(targetBeforeId);
                }

                lineId = document.getElementById("editingLine").textContent;

                line = document.getElementById(lineId);
                if (line !== null) {
                    line.setAttribute("x2", coord.x);
                    line.setAttribute("y2", coord.y);    
                }
            } 
            else {
                 selectedElement.setAttribute("x", coord.x);
                 selectedElement.setAttribute("y", coord.y);
                
                if (id.includes("concept") ||
                    id.includes("phrase")) {

                    resizeMap(selectedElement, coord);

                    let lines = getSourceTargetLines(selectedElement);
                    moveLines(lines, centerSelected.x, centerSelected.y);
                }
            }

            targetBefore.textContent = targetId;
        }
    }

    function endDrag(evt) {

        var action = document.getElementById("action");
        var targetId = evt.target.id;
        var targetBefore = document.getElementById("targetBefore");
        var targetBeroreId = targetBefore.textContent;
        var inEditionId = getInEditionId();

        if (action.textContent === "connection") {

            setDefaultClass(targetId);
            setDefaultClass(targetBeroreId);

            var editingLine = document.getElementById("editingLine");
            
            if (targetId.includes("line")) {

                if (targetBefore.textContent.includes("concept") ||
                    targetBefore.textContent.includes("phrase")) {
                    targetId = targetBefore.textContent;
                }
            }

            //console.log("endDrag: targetId", targetId);
    
            if (targetId.includes("phrase")) {
                
                if (inEditionId.includes("concept")) {
                    var connection = new Connection();
                    connection.sourceId = inEditionId;
                    connection.targetId = targetId;
                    connection.lineId = editingLine.textContent;
                    addConnection(connection);
                } else {
                    let line = document.getElementById(editingLine.textContent);
                    line.parentNode.removeChild(line);                
                }

            } else if (targetId.includes("concept")) {

                if (inEditionId.includes("phrase")) {
                    var connection = new Connection();
                    connection.sourceId = inEditionId;
                    connection.targetId = targetId;
                    connection.lineId = editingLine.textContent;
                    addConnection(connection);
                    finishEditions();
                }
                else {
                    finishEditions();
                    var connection = new Connection();
                    connection.sourceId = inEditionId;
                    connection.targetId = targetId;
                    connection.lineId = editingLine.textContent;

                    createPhrase(connection);
                }


            } else {

                let line = document.getElementById(editingLine.textContent);
                
                if (line !== null) {

                    if (inEditionId.includes("phrase")) {
                        divToSvgPhrase();
                    }
                    line.parentNode.removeChild(line);                
                }
            }

            divToSvgConcept();
            setMinSize();

        } else if (action.textContent === "delete") {

             if (targetId.includes("concept") ||
                 targetId.includes("phrase")) {

                 let conceptOrPhrase = document.getElementById(targetId);
                 deleteElement(conceptOrPhrase);

            } else if (targetId.includes("line")) {

                let line = document.getElementById(targetId);
                //console.log("endDrag: line", line);
                deleteElement(line);
            }
    
            resetTrash();
            setAction("move");
        }

        selectedElement = null;

        setMinSize();
    }
}
