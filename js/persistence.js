/**
 * Changes trash for logo image.
 */
window.hideTrashShowLogo = function() {
    document.getElementById("logoIn").setAttribute("class", "trash");
    document.getElementById("trash").setAttribute("class", "invisible");
}

/*
 * https://stackoverflow.com/questions/28899298/extract-the-text-out-of-html-string-using-javascript
 */
window.extractMapTextWords = function(htmlText) {
    var mapTextWords = document.getElementById("mapTextWords");
    mapTextWords.innerHTML = htmlText;
    mapTextWords.innerHTML = mapTextWords.textContent || mapTextWords.innerText;
    mapTextWords.innerHTML = mapTextWords.innerHTML.replace(/&nbsp;/g, ' ');
    mapTextWords.innerHTML = mapTextWords.innerHTML.replace(/[^a-zA-Z0-9àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ ]/g, " ");
    //console.log("extractMapTextWords: text", mapTextWords.innerHTML);
    return mapTextWords.innerHTML;
}

window.getMapTextContent = function() {

    let element = document.getElementsByClassName("ql-editor")[0];
    if (element === undefined) {
        return;
    }

    return document.getElementsByClassName("ql-editor")[0].innerHTML;
}

window.setMapTextContent = function(content) {
    document.getElementsByClassName("ql-editor")[0].innerHTML = content;
    extractMapTextWords(content);
}

window.valueToTextContent = function(idFrom, idTo) {

    document.getElementById(idTo).textContent = 
    document.getElementById(idFrom).value;
}

window.textContentToValue = function(idFrom, idTo) {
    document.getElementById(idTo).value = 
    document.getElementById(idFrom).textContent;
}

window.copyInputFields = function() {

    valueToTextContent("mapName", "mapNameCopy");
    valueToTextContent("mapFocus", "mapFocusCopy");
    valueToTextContent("mapAuthor", "mapAuthorCopy");
    valueToTextContent("mapAuthorEmail", "mapAuthorEmailCopy");

    document.getElementById("mapContextCopy").textContent = document.querySelector('input[name="mapContext"]:checked').value;
    document.getElementById("specificContextCopy").textContent = document.getElementById("specificContext").value;    
    document.getElementById("mapTextCopy").textContent = getMapTextContent();
}

window.restoreInputFields = function() {

    textContentToValue("mapNameCopy", "mapName");
    textContentToValue("mapFocusCopy", "mapFocus");
    textContentToValue("mapAuthorCopy", "mapAuthor");
    textContentToValue("mapAuthorEmailCopy", "mapAuthorEmail");
    
    let content = document.getElementById("mapTextCopy").textContent;
    setMapTextContent(content);

    const mapContextCopyContent      = document.getElementById("mapContextCopy").textContent;
    const specificContextCopyContent = document.getElementById("specificContextCopy").textContent;  
    let specificContext = document.getElementById("specificContext");
    specificContext.value = specificContextCopyContent;

    if (mapContextCopyContent === "general") {
    
        document.getElementById("mapContextGeneral").checked = true;
        specificContext.setAttribute("class", "hide");
        
    } else if (mapContextCopyContent === "specific") {
    
        document.getElementById("mapContextSpecific").checked = true;
        specificContext.setAttribute("class", "input-inline");
    }

}

/**
 * Invoked by Photograh buttom.
 */
 window.saveAsImage = function() {
    //hideTrashShowLogo();
    createDownloadImage();
}

window.createFileName = function(extension) {

    var fileName = document.getElementById("mapName").value.replaceAll("?", "").trim();
    var specificContext = document.getElementById("specificContext").value.replaceAll("?", "").trim();

    if (specificContext.length > 0) {
        fileName = fileName + "-" + specificContext;
    }

    if (fileName.length > 0) {
        fileName = fileName + "." + extension;
    } else {
        fileName = "vidya-maps." + extension;
    }

    return fileName;
}

window.createDownloadImage = function() {

    let cmapd = document.getElementById("cmapd");

    const fileName = createFileName("png");

    domtoimage.toJpeg(cmapd, { quality: 0.95 }).then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = fileName;
        link.href = dataUrl;
        link.click();
    });
}

/**
* Serialize the concept map as XML.
* Save the serialized XML into the Operating System. 
*/
window.saveAsXml = function() {

    copyInputFields();

    const fileName = createFileName("vm1");

    const xmls = new XMLSerializer(); 
    const cmapd = document.getElementById("cmapd");
    const xml = xmls.serializeToString(cmapd);
            
    var bb = new Blob([xml], {type: 'text/plain'});
    let link = document.createElement("a");
    
    link.href = window.URL.createObjectURL(bb);
    link.download = fileName;
    link.click();
}

/**
 * Invoked by Open buttom.
 * @param {*} event 
 */
window.loadXmlFromOS = function(event) {

    var file = event.target.files[0];
    if (!file) {
        return;
    }

    var reader = new FileReader();
    reader.onload = function (event) {
        var contents = event.target.result;
        let pos = contents.indexOf(">");
        contents = contents.substring(pos+1);
        contents = contents.slice(0, -6);
        console.log("contents", contents);
        let cmapd = document.getElementById("cmapd");
        cmapd.innerHTML = contents;
        restoreInputFields();
        updateReport();
    };
    reader.readAsText(file);
};


